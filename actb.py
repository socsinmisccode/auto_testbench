#!/bin/python
import sys
import re

def delSpacesFront(base: str):
    for i in range(0, len(base)):
        if (base[i] != ' '):
            return base[i:]
def delRegFront(base: str):
    if(base[:3] == 'reg'):
        return base[3:]
    else:
        return base

dest = None
inputfileName = None
options = {'dumpfile': False, 'outputFileName': None, 'indentSpaces': 4, 'monitoron': False}
userArgv = sys.argv[1:]
for i in range(0,len(userArgv)):
    curOpt = userArgv[i]
    if(curOpt == None):
        continue
    elif(curOpt[0] == '-'):
        for j in curOpt[1:]:
            if(j == 'd'):
                options['dumpfile'] = True
            elif(j == 'm'):
                options['monitoron'] = True
            elif(j == 'o'):
                try:
                    options['outputFileName'] = userArgv[i + 1]
                    userArgv[i + 1] = None
                except:
                    print("There is an error at -o option. You may need to set a output file name.")
                    sys.exit()
            elif(j == 'i'):
                try:
                    options['indentSpaces'] = ord(userArgv[i + 1]) - ord('0')
                    userArgv[i + 1] = None
                except:
                    print("Please input indent spaces you want.")
                
    elif (type(curOpt)==str):
        inputfileName = curOpt

# Emerge inputfilename
if(inputfileName == None):
    print("Please give a filename.")
    sys.exit()

# Emerge outputfilename
if(options['outputFileName'] == None):
    outputFileName = inputfileName
    outputFileName = outputFileName[:outputFileName.find('.')] + '_tb.v'
    options['outputFileName'] = outputFileName
try:
    dest = open('./' + inputfileName, mode='r').read()
except:
    print("Cannot read the file. Perhaps you need to create one.")
    sys.exit()

# Emerge indentblock
indentBlock = ""
for i in range(0, options['indentSpaces']):
    indentBlock += " "

# Get args
try:
    statement = re.findall('`[^\\n]*timescale[^\\n]+', dest)
    
    moduleName = re.search('(?<=module)[^(]+', dest).group()
    moduleName = re.search('[^\s]+', moduleName).group()
    allArgs = re.search(moduleName + '[\s]*[^)]+', dest).group()
    allArgs = allArgs[allArgs.find('(') + 1:]

    inputArgs = re.findall('(?<=input)[\s]+\[*[^,\\n\[]+', allArgs)
    inputArgs = [ delSpacesFront(i) for i in inputArgs ]

    outputArgs = re.findall('(?<=output)[\s(reg)]+\[*[^,\\n\[]+', allArgs)
    outputArgs = [ delRegFront(delSpacesFront(i)) for i in outputArgs ]

    inputVars = [ delSpacesFront(i[i.find(']') + 1:]) for i in inputArgs ]
    outputVars = [ delSpacesFront(i[i.find(']') + 1:]) for i in outputArgs ]
    allVars = inputVars + outputVars
except:
    print("Some Error.")

# Emerge content

# Emerge statement
content = ''
for i in statement:
    content += i + '\n'

# Emerge module    
content += '''
module {module}_tb;

{indent}///Inputs
'''.format(module=moduleName, indent=indentBlock)

# Emerge inputArgs
for i in inputArgs:
    content += '{indent}reg {inputWire};\n'.format(inputWire=i, indent=indentBlock)

# Emerge outputs
content += '''
{indent}///Outputs
'''.format(indent = indentBlock)

# Emerge outputArgs
for i in outputArgs:
    content += '{indent}wire {outputWire};\n'.format(outputWire=i, indent=indentBlock)

# Emerge module to test
content += '''
{indent}///Instantiate the Unit Under Test (UUT)
{indent}{module} uut (
'''.format(module=moduleName, indent=indentBlock)

# Connect Vars
for i in range(0, len(allVars)):
    content += '{indent}{indent}.{varName}({varName})'.format(varName=allVars[i], indent=indentBlock)
    if(i != len(allVars) - 1):
        content += ',\n'
    else:
        content += '\n'

# Emerge the initial block
content += '''{indent});

{indent}/// Add your code here.
{indent}initial begin
'''.format(indent=indentBlock)

#Emerge monitoron
if(options['monitoron'] == True):
    content += '{indent}{indent}$monitoron;\n'.format(indent = indentBlock)

# Emerge dumpfile
if(options['dumpfile'] == True):
    content += '''{indent}{indent}$dumpfile("{modulename}");
{indent}{indent}$dumpvars(0, {modulename}_tb);
'''.format(modulename=moduleName, indent=indentBlock)

content += '{indent}{indent}$finish();\n'.format(indent=indentBlock)
content += '''{indent}end

endmodule
'''.format(indent=indentBlock)

# Write file
outputFile = open('./' + options['outputFileName'], mode="w")
outputFile.write(content)
outputFile.close()
