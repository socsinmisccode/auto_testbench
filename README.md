## Order
Auto create verilog testbench file with .v file
## Usage
```
python actb.py [-options] filename.v
```
Will create `filename_tb.v`
## Options
### -d
Create
```
$dumpfile("{modulename}");
$dumpvars(0, {modulename}_tb);
```
in the initial block
### -o
Output the result to special file.

usage:
```
-o filename.v
```
### -i
Set the space numbers of indentation.
Default value is `4`.

usage:
```
-i $N
```
